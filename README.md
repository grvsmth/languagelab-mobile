# README #

This is a prototype mimicry application, of the type used in language labs, for Android.  It allows the user to play back an audio file, record a new audio file, and then listen to the original and new files in sequence.  Future versions will integrate over HTTP with an online library of lessons, exercises and media files.