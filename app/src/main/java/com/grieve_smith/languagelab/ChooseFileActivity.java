package com.grieve_smith.languagelab;

import android.app.ListActivity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChooseFileActivity extends ListActivity {

    // http://www.dreamincode.net/forums/topic/190013-creating-simple-file-chooser/

    private static final String TAG = "ChooseFileActivity";

    private File currentDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
    private FileArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fill(currentDir);
    }

    private void fill (File f) {
        File[] dirs = f.listFiles();
        this.setTitle("Current dir: " + f.getName());
        List<Option> dir = new ArrayList<>();
        List<Option> fls = new ArrayList<>();

        try {
            for (File ff: dirs) {
                if (ff.isDirectory()) {
                    dir.add(new Option(ff.getName(), "Folder", ff.getAbsolutePath()));
                } else {
                    fls.add(new Option(ff.getName(), "File size: " + ff.length(), ff.getAbsolutePath()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.sort(dir);
        Collections.sort(fls);
        dir.addAll(fls);
//        if (!f.getName().equalsIgnoreCase("sdcard")) {
            dir.add(0, new Option("..", "Parent Directory", f.getParent()));
//        }

        adapter = new FileArrayAdapter(ChooseFileActivity.this, R.layout.activity_choose_file, dir);
        this.setListAdapter(adapter);
    }

    private void onFileClick (Option o) {
        Toast.makeText(this, "File clicked: " + o.getName(), Toast.LENGTH_SHORT).show();
        // TODO return info to MainActivity
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.putExtra("FILEPATH",o.getPath());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_choose_file, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Option o = adapter.getItem(position);
        if (o.getData().equalsIgnoreCase("folder") || o.getData().equalsIgnoreCase("parent directory")) {
            currentDir = new File(o.getPath());
            fill(currentDir);
        } else {
            onFileClick(o);
        }
    }
}
