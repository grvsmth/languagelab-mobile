package com.grieve_smith.languagelab;

import java.net.URI;

/**
 * Created by grvsmth on 9/19/2015.
 */
public class Exercise {
    private int id;
    private String name;
    private String instructions;
    private String modelFile;
    private URI modelURI;

    public Exercise(int id, String name, String instructions, String modelFile, URI modelURI) {
        this.id = id;
        this.name = name;
        this.instructions = instructions;
        this.modelFile = modelFile;
        this.modelURI = modelURI;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getModelFile() {
        return this.modelFile;
    }
    public void setModelFile(String modelFile) {
        this.modelFile = modelFile;
    }

    public URI getModelURI() {
        return modelURI;
    }

    public void setModelURI(URI modelURI) {
        this.modelURI = modelURI;
    }

}
