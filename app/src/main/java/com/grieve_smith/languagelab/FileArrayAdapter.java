package com.grieve_smith.languagelab;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by grvsmth on 8/11/2015.
 */
public class FileArrayAdapter extends ArrayAdapter<Option> {

    private Context c;
    private int id;
    private List<Option> items;

    public FileArrayAdapter (Context context, int textViewResourceId, List<Option> objects) {
        super(context, textViewResourceId, objects);
        c = context;
        id = textViewResourceId;
        items = objects;
    }

    public Option getItem(int i) {
        return items.get(i);
    }

    @Override
    public View getView (int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (null == v) {
            LayoutInflater vi = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(id, null);
        }
        final Option o = items.get(position);
        if (null != o) {
            TextView t1 = (TextView) v.findViewById(R.id.TextView11);
            TextView t2 = (TextView) v.findViewById(R.id.TextView12);

            if (null != t1) {
                t1.setText(o.getName());
            }
            if (null != t2) {
                t2.setText(o.getData());
            }

        }
        return v;
    }
}
