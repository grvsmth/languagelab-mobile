package com.grieve_smith.languagelab;

import java.util.List;

/**
 * Created by grvsmth on 9/19/2015.
 */
public class Lesson {

    private int id;
    private String title;
    private String description;
    private List<Lesson> lessonList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Lesson> getLessonList() {
        return lessonList;
    }

    public void setLessonList(List<Lesson> lessonList) {
        this.lessonList = lessonList;
    }

    public Lesson(int id, String title, String description, List<Lesson> lessonList) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.lessonList = lessonList;
    }
}
