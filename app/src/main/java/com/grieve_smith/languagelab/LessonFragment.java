package com.grieve_smith.languagelab;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.grieve_smith.languagelab.dummy.DummyContent;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p>
 * Activities containing this fragment MUST implement the {@link OnLessonListener}
 * interface.
 */
public class LessonFragment extends Fragment implements AbsListView.OnItemClickListener {

    private static final String TAG = "LessonFragment";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private File currentDir;
    private File documentsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
    private File downloadsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    private File externalDir = Environment.getExternalStorageDirectory();
    private File[] storageDirs = { documentsDir, downloadsDir, externalDir };

    private TextView titleView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String titleText;

    private OnLessonListener mListener;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private FileArrayAdapter mAdapter;

    // TODO: Rename and change types of parameters
    public static LessonFragment newInstance(String param1, String param2) {
        LessonFragment fragment = new LessonFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public LessonFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        for (File tryDir: storageDirs) {
            if (null != tryDir) {
                currentDir = tryDir;
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lesson, container, false);

        titleView = (TextView) view.findViewById(R.id.fragmenttitle);

        if (null == currentDir) {
            titleText = "Can't access external storage directory!";
            titleView.setText(titleText);
        } else {
            fill(currentDir);
            // Set the adapter
            mListView = (AbsListView) view.findViewById(R.id.lessonList);
            if (null == mAdapter) {
                titleText = "Error: mAdapter is null!";
                titleView.setText(titleText);
            } else {
                ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

                // Set OnItemClickListener so we can be notified on item clicks
//                mListView.setOnItemClickListener(this);
                mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.i(TAG, "Item clicked!");
                    }
                });
            }
        }

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnLessonListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.i(TAG, "Item clicked!");
        Option o = mAdapter.getItem(position);
        Toast.makeText(getActivity(), "File clicked: " + o.getName(), Toast.LENGTH_SHORT).show();
        if (o.getData().equalsIgnoreCase("folder") || o.getData().equalsIgnoreCase("parent directory")) {
            currentDir = new File(o.getPath());
            fill(currentDir);
        } else if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.

            Toast.makeText(getActivity(), "File clicked: " + o.getName(), Toast.LENGTH_SHORT).show();

//            mListener.onLessonSelected(o);
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnLessonListener {
        // TODO: Update argument type and name
        public void onLessonSelected(String fileName);
    }

    private void fill (File f) {
        File[] dirs = f.listFiles();

        titleText = "Current dir: " + f.getName();

        titleView.setText(titleText);
        List<Option> dir = new ArrayList<>();
        List<Option> fls = new ArrayList<>();

        try {
            for (File ff: dirs) {
                if (ff.isDirectory()) {
                    dir.add(new Option(ff.getName(), "Folder", ff.getAbsolutePath()));
                } else {
                    fls.add(new Option(ff.getName(), "File size: " + ff.length(), ff.getAbsolutePath()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.sort(dir);
        Collections.sort(fls);
        dir.addAll(fls);
//        if (!f.getName().equalsIgnoreCase("sdcard")) {
        dir.add(0, new Option("..", "Parent Directory", f.getParent()));
//        }

        mAdapter = new FileArrayAdapter(getActivity(), R.layout.fragment_lesson, dir);

    }
}
