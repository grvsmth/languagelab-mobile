package com.grieve_smith.languagelab;

import android.annotation.TargetApi;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements LessonFragment.OnLessonListener {

    private static final String TAG = "MainActivity";

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd-HHMMss");

    MediaPlayer mediaPlayer = new MediaPlayer();
    MediaRecorder recorder = new MediaRecorder();

    // TODO check to see if external storage is available

    // TODO allow users to set custom input and output directories
    private File outputDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
    private File inputDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);

    private String inputFileName = "";
    private String outputFileName = "";

    private Random random = new Random();
    private DecimalFormat integerFormat = new DecimalFormat("#");
    private boolean isRecording = false;
    private Date now;
    private LessonFragment lessonFragment;
    private FragmentTransaction transaction;

/*
    @Override
    protected void onResume(Bundle savedInstanceState) {
        super.onResume(savedInstanceState);
    }
*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Uri uri;
        TextView textView1 = (TextView) findViewById(R.id.textView);
        TextView textView2 = (TextView) findViewById(R.id.textview2);

        textView2.setText("Input directory: " + inputDir);

        final Button playButton = (Button)findViewById(R.id.playButton);
        final Button recButton = (Button) findViewById(R.id.recButton);
        final Button mimicButton = (Button) findViewById(R.id.mimicButton);

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            inputFileName = extras.getString("FILEPATH");
            textView2.setText(inputFileName);
            uri = Uri.fromFile(new File(inputFileName));
            try {
                mediaPlayer.setDataSource(getBaseContext(), uri);
                mediaPlayer.prepare();
                playButton.setEnabled(true);
                mimicButton.setEnabled(true);

                // TODO check for free space

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        playButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (playButton.isEnabled()) {
                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                        mediaPlayer.pause();
                        recButton.setEnabled(true);
                        playButton.setBackgroundColor(getResources().getColor(android.R.color.background_light));
                        mimicButton.setEnabled(true);
                    } else {
                        mediaPlayer.start();
                        playButton.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
                        recButton.setEnabled(false);
                    }
                }
            }
        });

        recButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (recButton.isEnabled()) {
                    if (isRecording) {
                        recorder.stop();
                        isRecording = false;
                        recButton.setActivated(false);
                        recButton.setBackgroundColor(getResources().getColor(android.R.color.background_light));
                        if (mediaPlayer != null) {
                            playButton.setEnabled(true);
                            mimicButton.setEnabled(true);
                        }
                    } else {
                        outputFileName = newOutFileName();
                        recorder.setOutputFile(outputFileName);
                        try {
                            recorder.prepare();
                        } catch (IOException e) {
                            Log.e(TAG, "Unable to prepare recorder");
                        }
                        isRecording = true;
                        playButton.setEnabled(false);
                        mimicButton.setEnabled(false);
                        recButton.setActivated(true);
                        recButton.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
                        recorder.start();
                    }
                }
            }
        });

        mimicButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (isRecording) {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                    }
                    recorder.stop();
                    mimicButton.setText(R.string.mimic);
                    mimicButton.setBackgroundColor(getResources().getColor(android.R.color.background_light));
                    isRecording = false;
                    recButton.setEnabled(true);
                    playButton.setEnabled(true);
                } else {
                    if (!mediaPlayer.isPlaying()) {
                        isRecording = true;
                        playButton.setEnabled(false);
                        recButton.setEnabled(false);
                        mimicButton.setText(R.string.playing);
                        mimicButton.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
                        outputFileName = newOutFileName();
                        recorder.setOutputFile(outputFileName);
                        try {
                            recorder.prepare();
                        } catch (IOException e) {
                            Log.e(TAG, "Unable to prepare recorder");
                        }
                        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {
                                mimicButton.setText(R.string.recording);
                                mimicButton.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
                                recorder.start();
                            }
                        });
                        mediaPlayer.start();
                    }
                }

            }
        });

        // TODO stop button

        // TODO play both button
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (R.id.action_open_lesson == id) {
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.maincontainer, new LessonFragment());
            transaction.addToBackStack(null);
            transaction.commit();
            return true;
        } else if (R.id.action_choose_file == id) {
            Intent intent = new Intent (getBaseContext(), ChooseFileActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String randomSuffix () {
        return Integer.toString(Math.round(random.nextFloat() * 1000));
    }

    private String newOutFileName () {
        // http://stackoverflow.com/questions/8182041/android-get-date-and-insert-to-filename
        now = new Date();
        return(outputDir + File.separator + formatter.format(now) + "-" + randomSuffix() + ".aac");
    }

    @Override
    public void onLessonSelected(String fileName) {

    }
}
